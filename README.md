WindowsWM - Cygwin/X rootless window management extension.
----------------------------------------------------------

WindowsWM is a simple library designed to interface with the
Windows-WM extension.  This extension allows X window managers to
better interact with the Cygwin XWin server when running X11 in a
rootless mode.

All questions regarding this software should be directed at the
Xorg mailing list:

  https://lists.x.org/mailman/listinfo/xorg

The master development code repository can be found at:

  https://gitlab.freedesktop.org/xorg/lib/libWindowsWM

Please submit bug reports and requests to merge patches there.

For patch submission instructions, see:

  https://www.x.org/wiki/Development/Documentation/SubmittingPatches

